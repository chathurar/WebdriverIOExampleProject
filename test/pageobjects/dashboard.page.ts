import Page from './page';

const SELECTORS = {
 
  header: "h1",
  
};

export default class DashboardPage extends Page {
  constructor() {
    super();
    $(SELECTORS.header).waitForDisplayed();
  }

  getBookSpaceLabel(): string {
    return $(SELECTORS.header).getText();
  }

}

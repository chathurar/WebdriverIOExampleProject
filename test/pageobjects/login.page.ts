import Page from './page';
import DashboardPage from './dashboard.page';

export default class LoginPage extends Page {
    get inputUsername () { return $(`#txtUsername`) }  // id
    get inputPassword () { return $(`//input[@name='txtPassword']`) }  //xpath
    get btnSubmit () { return $(`.button`) } // className

    login (username: string, password: string): DashboardPage {
        this.inputUsername.setValue(username);
        this.inputPassword.setValue(password);
        this.btnSubmit.click();
        return new DashboardPage();
    }

    open () {
        return super.open();
    }
}

